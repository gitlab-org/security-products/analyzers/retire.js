This project's issue tracker has been disabled, if you wish to [create an issue or bug please follow these directions](/CONTRIBUTING.md#issue-tracker).


**REMOVAL WARNING:** This analyzer has been replaced by [Gemnasium](https://gitlab.com/gitlab-org/security-products/analyzers/gemnasium) and is no longer in use as of GitLab 15.0. For more information, see the [removal issue](https://gitlab.com/gitlab-org/gitlab/-/issues/289830).

# Retire.js analyzer

Dependency Scanning for JavaScript projects. It's based on [Retire.js](https://retirejs.github.io/retire.js/).

This analyzer is written in Go using
the [common library](https://gitlab.com/gitlab-org/security-products/analyzers/common)
shared by all analyzers.

The [common library](https://gitlab.com/gitlab-org/security-products/analyzers/common)
contains documentation on how to run, test and modify this analyzer.

## How to update the upstream Scanner

- Check for the latest version at https://github.com/RetireJS/retire.js/tags and https://www.npmjs.com/package/retire (versions tab)
- Compare with the value of `SCANNER_VERSION` in the [Dockerfile](./Dockerfile)
- If an update is available, create a branch where `SCANNER_VERSION` is updated and open a Merge Request.
- Check for possible new security vulnerabilities by following the process in [our handbook](https://about.gitlab.com/handbook/engineering/development/secure/composition-analysis/#security-checks-when-updating-an-upstream-scanner).
- Check for possible license update by following the process in [our handbook](https://about.gitlab.com/handbook/engineering/development/secure/composition-analysis/#license-check-when-updating-an-upstream-scanner).

## Contributing

Contributions are welcome, see [`CONTRIBUTING.md`](CONTRIBUTING.md) for more details.

## License

This code is distributed under the MIT license, see the [LICENSE](LICENSE) file.

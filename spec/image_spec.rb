require "English"

describe "running image" do
  def image_name
    ENV.fetch("TMP_IMAGE", "retire.js:latest")
  end

  it "includes all the necessary system dependencies to allow a native module to be compiled" do
    `docker run -t --rm #{image_name} npm install cld`
    exit_code = $CHILD_STATUS.to_i
    expect(exit_code).to be 0
  end
end
